package utilities;

public class MatrixMethod {
	
	public int[][] duplicate(int[][] arr2d) {
		int arrLength = 0;
		int [][] copyArr = new int[arr2d.length][];
		for (int i=0; i<arr2d.length; i++) {
			arrLength = arr2d[i].length*2;
			int tempVar = 0;
			int[] tempArr = new int[arrLength];
			for (int j=0; j<arr2d[i].length; j++) {
				tempArr[tempVar] = arr2d[i][j];
				tempVar++;
				tempArr[tempVar] = arr2d[i][j];
				tempVar++;
			}
			copyArr[i] = tempArr;
		}
		return copyArr;
	}
	
}
