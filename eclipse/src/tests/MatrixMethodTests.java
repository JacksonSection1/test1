package tests;

import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	public void duplicateTest() {
		MatrixMethod tester = new MatrixMethod();
		int [][] inputArr = {{3,5,6}, {4,5,6}};
		int [][] testerArr = tester.duplicate(inputArr);
		assertArrayEquals(new int[][]{{3,3,5,5,6,6}, {4,4,5,5,6,6}}, testerArr);
	}
	
	@Test
	public void duplicateUnequalTest() {
		MatrixMethod tester = new MatrixMethod();
		int [][] inputArr = {{3}, {2,4,5,6}};
		int [][] testerArr = tester.duplicate(inputArr);
		assertArrayEquals(new int[][] {{3,3}, {2,2,4,4,5,5,6,6}}, testerArr);
	}
}
